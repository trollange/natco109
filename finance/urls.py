from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^download/(?P<id>[A-Za-z0-9\-]+)/natco-receipt.png$', views.receipt, name='receipt'),
    url(r'^download/(?P<id>[A-Za-z0-9\-]+)/natco-invoice.pdf$', views.invoice, name='invoice'),
]
