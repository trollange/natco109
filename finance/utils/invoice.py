from django.conf import settings

import requests


def generate_invoice(delegate, number=1):
    """
    Using some API to generate invoices, way easier than PDF generation,
    tho I dislike relying on some random web service.

    TODO:
    - handle potential Http429 error
      <https://github.com/Invoiced/invoice-generator-api#rate-limiting>
    - put international IBAN for intl delegates
    """
    data = {
        # # TEMPLATE DUAL-LANG - formatting fucked up
        # 'header': 'INVOICE / FAKTURA',
        # 'to_title': 'Client / Klijent',
        # 'date_title': 'Date / Datum',
        # 'due_date_title': 'Due date / Rok za plaćanje',
        # 'item_header': 'Item / Opis',
        # 'quantity_header': 'QTY / kol',
        # 'unit_cost_header': 'Price / Cena',
        # 'amount_header': 'Total / Ukupno',

        # TEMPLATE DUAL-LANG - formatting fucked up
        # 'header': 'FAKTURA',
        # 'to_title': 'Klijent',
        # 'date_title': 'Datum',
        # 'due_date_title': 'Rok za plaćanje',
        # 'item_header': 'Opis',
        # 'quantity_header': 'Količina',
        # 'unit_cost_header': 'Cena',
        # 'amount_header': 'Zbirno',
        # # 'total_title': 'Ukupno za naplatu',
        # 'notes_title': 'Napomena',
        # 'subtotal_title': 'Međuzbir',
        # 'balance_title': 'Iznos za naplatu',

        "from": '\n'.join([
            "AIESEC Lokalni komitet Niš",
            "Trg Kralja Aleksandra 11, 18000 Niš",
            "Matični broj: 17100513",
            "PIB: 100668613",
            "Žiro račun: 160-129610-22 - Banca Intesa",
        ]),
        "logo":"https://natco.aiesec.org.rs/assets/img/natco_black.png",
        "notes": '\n'.join([
            "Iznos se plaća u tri rate:",
            " - 4,000 dinara do 13.02.2017. godine,",
            " - 4,000 dinara do 24.02.2017. godine,",
            " - 4,550 dinara do 10.03.2017. godine.",
            "",
            "U slučaju plaćanja punog iznosa odjednom, rok za uplatu je 13.02.2017. godine.",
            "",
            "U slučaju neblagovremenog izmirenja obaveza, obračunava se zatezna kamata od 2,800 dinara.",
            "",
            "AIESEC Lokalni komitet Niš nije PDV obveznik.",
            "Faktura je važeća bez pečata i potpisa.",
        ]),
        "currency": "RSD",

        "to":"{d.name} {d.surname}".format(d=delegate),
        "number": "N15/17",
        "items": [
            {
                "name": "Pun pansion",
                "quantity": delegate.nights_booked(),
                "unit_cost": settings.PRICING['accommodation'],
            },
        ],

    }
    if delegate.dtype == delegate.STANDARD:
        fee = settings.PRICING['fee']
    elif delegate.dtype == delegate.INTL:
        fee = settings.PRICING['fee'] + settings.PRICING['intladdedfee']
    elif delegate.dtype == delegate.PARTY:
        fee = settings.PRICING['partyfee']

    data['items'].append({'name': 'Organizacioni troškovi', 'quantity': 1, 'unit_cost': fee})

    if delegate.gala:
        data['items'].append({'name': 'Gala veče', 'quantity': 1, 'unit_cost': settings.PRICING['gala']})

    if delegate.tshirt:
        data['items'].append({'name': 'Majica', 'quantity': 1, 'unit_cost': settings.PRICING['tshirt']})

    inv = requests.post("https://invoice-generator.com", json=data)

    return inv.content
