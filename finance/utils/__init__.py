from django.conf import settings

from .invoice import generate_invoice
from .receipt import generate_receipt
from public.utils import current_round, round_by_date


def sum_for_invoice(delegate):
    apprnd = round_by_date(delegate.applied_on)

