from django.conf import settings
from django.forms import (
    BooleanField,
    ModelForm,
    ValidationError,
    modelform_factory,
)

from datetime import date

from .models import Delegate, LC, MC


default_fields = (
    'name',
    'surname',
    'gender',
    'dob',
    'phone',
    'email',
    'fb',
    'emergency',
    'lc',
    'food_req',
    'allergies',
    'expectations',
    'comments',

    'arrival',
    'departure',
    'gala',
    # 'tshirt',

    'dtype',
)
standard_fields = ('uni', 'track', 'position', 'transport', )
intl_fields = ('place_arr_dep', 'pre_conf_accommodation', )
party_fields = ('attendance_count', )


"""
TODO:
- all previous Serbian LCs for party delegates in form validation
- check MC/LC values in form validation (LC for standard, MC:LC relation for intl)
- on save() check form type
"""


class BaseDelegateForm(ModelForm):
    agreement = BooleanField(required=True)

    def __init__(self, *args, **kwargs):
        super(BaseDelegateForm, self).__init__(*args, **kwargs)

        if isinstance(self, StandardDelegateForm):
            self.type = Delegate.STANDARD
        elif isinstance(self, IntlDelegateForm):
            self.type = Delegate.INTL
        elif isinstance(self, PartyDelegateForm):
            self.type = Delegate.PARTY
        else:
            raise TypeError("BaseDelegateForm is an abstract class")

        # Let's set the fields as required according to form type
        if self.type == Delegate.STANDARD:
            fields = standard_fields
        elif self.type == Delegate.INTL:
            fields = standard_fields + intl_fields
        elif self.type == Delegate.PARTY:
            fields = party_fields
        else:
            # raise ValueError 
            fields = standard_fields #DEBUG ONLY

        for field in fields:
            self.fields[field].required = True

        # The following are BooleanFields. They don't pass validation by default
        # if they are provided with a false value. So, we alter the behavior.
        # Highly hackish. Would like to re-engineer this in the future.
        for field in ('gender', 'gala', 'transport', 'pre_conf_accommodation'):
            try:
                self.fields[field].required = False
            except KeyError:
                # Particular field not in this form
                pass




    def clean(self):
        # Clean with parent's method
        cleaned_data = super(BaseDelegateForm, self).clean()

        arrival = cleaned_data.get('arrival')
        departure = cleaned_data.get('departure')
        if arrival and departure:
            # In case of departure date set before arrival date
            if cleaned_data['arrival'] > self.cleaned_data['departure']:
                self.add_error('departure', "You cannot time travel, can you?")

        # LC, MC stuff
        lc = cleaned_data.get('lc')
        mc = cleaned_data.get('mc')
        if lc and self.type == Delegate.STANDARD:
            if lc.id not in LC.get_active_LC_ids():
                self.add_error('lc', "Invalid LC.")

        elif lc and self.type == Delegate.PARTY:
            if lc.id not in LC.get_serbian_LC_ids():
                self.add_error('lc', "Invalid LC.")

        elif lc and mc and self.type == Delegate.INTL:
            # Intl delegate
            # May look hackish, but we won't need that
            # data anymore. This is pretty much the simplest way.
            if not lc.mc == mc:
                self.add_error('lc', "Invalid LC or MC.")

        return cleaned_data


    def clean_arrival(self):
        # Check whether the provided date is between
        # the first day and last day of the conference
        arr = self.cleaned_data['arrival']
        if not (date(*settings.DATE_FIRST) <= arr <= date(*settings.DATE_LAST)):
            raise ValidationError("Arrival date out of range.")
        return arr

    def clean_departure(self):
        # Same as above with arrival date
        dep = self.cleaned_data['departure']
        if not (date(*settings.DATE_FIRST) <= dep <= date(*settings.DATE_LAST)):
            raise ValidationError("Departure date out of range.")
        return dep


    def clean_dtype(self):
        dtype = self.cleaned_data['dtype']
        if dtype not in (1, 2, 3):
            # Only on tampered requests
            raise ValidationError("Tampered request.")
        return dtype


StandardDelegateForm = modelform_factory(Delegate,
    form=BaseDelegateForm, fields=default_fields+standard_fields)

IntlDelegateForm = modelform_factory(Delegate,
    form=BaseDelegateForm, fields=default_fields+standard_fields+intl_fields)

PartyDelegateForm = modelform_factory(Delegate,
    form=BaseDelegateForm, fields=default_fields+party_fields) # TODO: All previous LCs!

resolver = {
    Delegate.STANDARD: StandardDelegateForm,
    Delegate.INTL:     IntlDelegateForm,
    Delegate.PARTY:    PartyDelegateForm,
}
